#include <iostream>
#include "my/vector.h"
#include <gtest/gtest.h>


// TEST(vector, da){

//     vector<char> a(5);
//     // int m[2];
//     // m[0]=3;
//     // m[1]=2;
//     // int *b = m;
//     a[0] = 'a';
//     a[2] = 'b';
//     std::cout << a.capacity() << std::endl;
//     std::cout << a.size();
//     // vector<char> b(std::move(a));

//     // for (char i : a){
//     //     std::cout << i << ' ';
//     // }
//     EXPECT_EQ(a[2], 'b');
//     // std::cout << std::endl;

//     // std::cout << a[2] << std::endl;
//     // std::cout << m << std::endl;
// }

// TEST(vector, push_back){
//     vector<Var> a;
//     Var t;
//     t.number = 1;
//     a.push_back(t);
//     std::cout << a[0].number;
// }

// TEST(vector, capacity){
//     vector<int> a(5);
//     a.push_back(1);
//     // a[2] = 3;
//     // a[0] = 2;
//     // std::cout << a[0] << std::endl;
//     // std::cout << a[2] << std::endl;
//     // std::cout << a.size() << std::endl;
//     // std::cout << a.capacity() << std::endl;
//     for(auto now : a){
//         std::cout << now << " ";
//     }
//     std::cout << std::endl;
//     a.shrink_to_fit();
//     // std::cout << a.capacity() << std::endl;
//     EXPECT_EQ(a.capacity(), 6);
// }

// TEST(vector, erase){
//     vector<int> a(5);
    // for(int i=0; i<5; i++){
    //     a[i] = i + 1;
    // }
//     a.erase(a.begin() + 1, a.begin() + 3);
//     for(auto now : a){
//         std::cout << now << " ";
//     }
//     std::cout << "/";
//     a.clear();
//     for(auto now : a){
//         std::cout << now << ",";
//     }
//     std::cout << "/";
//     std::cout << a.capacity();
//     // vector<int> b;
//     // b = {2,3,4,5};
//     // EXPECT_EQ(a, {2,3,4,5});
// }

// TEST(vector, skobki){
//     vector<int> a  = {1,2,3,4};
//     for (auto now : a){
//         std::cout << now;
//     }
// }

// TEST(vector, pop_back){
//     vector<int> a = {1,2,3,4,5};
//     a.pop_back();
//     vector<int> b = {1,2,3};
//     if(a != b){
//         std::cout << "not equal" << std::endl;
//     }

//     // EXPECT_EQ(a, b);
// }

// TEST(vector, equal){
//     vector<int> a = {1,2,3,4};
//     vector<int> b = {1,2,3,4};
//     if( a == b ){
//         std::cout << "equal" << std::endl;
//     }
// }

// TEST(vector, compare)
// {
//     vector<int> const less = {1, 2, 2, 4, 5};
//     vector<int> const a    = {1, 2, 3, 4, 5};
//     vector<int> const more = {1, 2, 3, 5, 5};

//     vector<int> b = {2,3,4};
//     vector<int> c = {1,2,3};

//     auto const allcmp = []<typename T>(vector<T> const &lhs, vector<T> const &rhs)
//         -> vector<bool>
//     {
//         return
//         {
//             lhs <  rhs,
//             lhs <= rhs,
//             lhs >  rhs,
//             lhs >= rhs,
//             lhs == rhs,
//             lhs != rhs,
//         };
//     };
//     EXPECT_EQ(allcmp(a,    a), (vector<bool>{false,  true, false,  true,  true, false}));
//     EXPECT_EQ(allcmp(a, less), (vector<bool>{false, false,  true,  true, false,  true}));
//     EXPECT_EQ(allcmp(a, more), (vector<bool>{ true,  true, false, false, false,  true}));
//     EXPECT_EQ(allcmp(a,    b), (vector<bool>{true, true, false, false,false ,  true}));
//     EXPECT_EQ(allcmp(a,    c), (vector<bool>{false, false, true, true, false, true}));
// }

// TEST(vector, insertion){
//     vector<int> a(5);
//     a[0] = 1;
//     a[1] = 2;
//     a[2] = 3;
//     a.insert(a.begin(),0);
//     // EXPECT_EQ(a, (vector<int>{0,1,2,3}));
    // for (auto now : a){
    //     std::cout << now << " ";
    // }
// }

// TEST(vector, erase){
//     // vector<Var> a(5);
//     // for(int i=0; i<5; i++){
//     //     a[i] = Var(i + 1);
//     // }

//     Var t(1);
//     std::cout << t.number << std::endl;

//     std::cout << "making vector" << std::endl;

//     vector<Var> a(2);

//     std::cout << "a[0] = t" << std::endl;

//     a[0] = t;

//     a[0].number = 2;

//     a[1].number = 5;

//     std::cout << "after a[0] = t" << std::endl;

//     std::cout << a[0].number  << " " << a[1].number << std::endl;

//     std::cout << "erase" << std::endl;

//     a.erase(a.begin(), a.end());

//     std::cout << "after erase" << std::endl;

//     std::cout << a.size() << " " << a.capacity() << std::endl;

// }

// TEST(vector, pop_back){
//     vector<Var> a(3);
//     // a.reserve(5);


//     Var t;
//     t.number = 2;
//     a[0] = t;
//     a[0].number = 1;
//     a[1].number = 3;
//     std::cout << "goin' to insert" << std::endl;
//     a.insert(a.begin(), t);

//     std::cout << "capacity = " << a.capacity() << " " << std::endl;

//     std::cout << a[0].number << " " << a[1].number << " " << a[2].number << " " << a[3].number << std::endl;

// }

// TEST(vector, insert_of_array){
//     vector<Var> a(3);

// 	Var t;
// 	t.number = 1;
// 	a[0] = t;

// 	a[0].number = 1;

//     a[1].number = 2;

//     Var m[3];

//     for(int i = 0; i < 3; i++){
//         m[i].number = i + 3;
//     }

//     for (auto now : a){
//         std::cout << now.number << " " << std::endl;
//     }

//     std::cout << "insert" << std::endl;

//     a.insert(a.end(), m, m + 3);

//     std::cout << "after insert" << std::endl;

//     for (auto now : a){
//         std::cout << now.number << " <- number" << std::endl;
//     }
// }

// TEST(vector, pop_back){
//     vector<Var> a;
//     // a.reserve(5);


//     Var t;
//     t.number = 2;
//     a.push_back(t);

//     a.insert(a.begin(), t);

// }

// TEST(vector, insert_for_count){
//     vector<Var> a(3);
//     vector<Var> b(3);

// 	a[0].number = 1;

//     a[1].number = 2;

//     a[2].number = 3;
    
// 	b[0].number = -1;

//     b[1].number = -2;

//     b[2].number = -3;


//     a.swap(b);

//     for (auto now : a){
//         std::cout << now.number << " <- a number" << std::endl;
//     }

//     for (auto now : b){
//         std::cout << now.number << " <- b number" << std::endl;
//     }


// }

TEST(vector, emplace1){
    vector<int> a(2);
    int m[3] = {1,2,3};
    a.assign(m, m + 3);
    std::cout << " " << a.capacity() << " " << a.size() << std::endl;

    for( auto now : a){
        std::cout << now << " ";
    }
    std::cout<< std::endl;


}

int main(int argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

